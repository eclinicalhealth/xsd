![logo_platform.png](https://bitbucket.org/repo/EKadr/images/1693606475-logo_platform.png)

### eClinicalHealth Clinpal XML Schemas ###
 
This repository contains the XML schemas. They can be obtain via raw URLs for inclusion in your metadata

eg: 

```
#!xml
<?xml version="1.0" encoding="UTF-8"?>
<Version xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:noNamespaceSchemaLocation="https://bitbucket.org/eclinicalhealth/xsd/raw/V0.1/FormDefinition.xsd"
         name="V1.0" kind="MY ECRF" versionType="STUDY_BOOK">
</Version>
```

To obtain the most recent version always use the following:
```
#!xml
<?xml version="1.0" encoding="UTF-8"?>
<Version xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:noNamespaceSchemaLocation="https://bitbucket.org/eclinicalhealth/xsd/raw/master/FormDefinition.xsd"
         name="V1.0" kind="MY ECRF" versionType="STUDY_BOOK">
</Version>
```

To include the account activation schema in a document use:
```
#!xml
<?xml version="1.0" encoding="UTF-8" ?>
<ActivationCodeConfiguration xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:noNamespaceSchemaLocation="https://bitbucket.org/eclinicalhealth/xsd/raw/master/activation-code-configuration.xsd">
</ActivationCodeConfiguration>
```

To include the worflow definition schema in a document use:
```
#!xml
<?xml version="1.0" ?>
<workflowDefinition xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        xsi:noNamespaceSchemaLocation="https://bitbucket.org/eclinicalhealth/xsd/raw/master/WorkFlowDefinition.xsd">
```